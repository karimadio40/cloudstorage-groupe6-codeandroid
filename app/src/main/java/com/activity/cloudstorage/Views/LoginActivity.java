package com.activity.cloudstorage.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activity.cloudstorage.Controle.LoginUser;
import com.activity.cloudstorage.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText user_pseudo, user_Password;
    TextView Register,passwordforget;
    Button btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user_pseudo = (EditText) findViewById(R.id.pseudo);
        user_Password = (EditText) findViewById(R.id.password);
        Register=findViewById(R.id.inscription);
        btnlogin = findViewById(R.id.connexion);
        passwordforget=findViewById(R.id.forgot_pass);
        btnlogin.setOnClickListener(this);

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {

        final String pseudo_user = user_pseudo.getText().toString().trim();
        final String password_user = user_Password.getText().toString().trim();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("Message", "run:Bouton ");

                LoginUser user=new LoginUser();
                user.doLoginUser(LoginActivity.this,pseudo_user,password_user);

            }
        }).start();
        passwordforget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(LoginActivity.this);
                LinearLayout layout = new LinearLayout(LoginActivity.this);
                View mview = getLayoutInflater().inflate(R.layout.password_forget_dielog, null);
                layout.setOrientation(LinearLayout.VERTICAL);
                layout.setOrientation(LinearLayout.HORIZONTAL);
                layout.setMinimumHeight(500);
                layout.setMinimumWidth(600);
                final EditText email_validate=mview.findViewById(R.id.password_i);
                final  EditText passwordUpdate=mview.findViewById(R.id.password_n);
                final Button button=mview.findViewById(R.id.send_it);
                final String email=email_validate.getText().toString().trim();
                final String password=passwordUpdate.getText().toString().trim();
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                LoginUser loginUser=new LoginUser();
                                loginUser.PasswordUpdating(LoginActivity.this,email,password);
                            }
                        }).start();
                    }
                });

                dialog.setContentView(mview);
                dialog.show();
            }
        });
    }



}