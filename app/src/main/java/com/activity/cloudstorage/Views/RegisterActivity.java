package com.activity.cloudstorage.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.activity.cloudstorage.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {


    private EditText pseudo, email, password, confirm_password;
    private TextView sign_in;
    private Button btnRegister;
    final String url_Register = "http://192.168.43.251/AndroidTP/Inscription.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        pseudo = (EditText) findViewById(R.id.pseudo);
        email = (EditText) findViewById(R.id.email);
        sign_in = findViewById(R.id.sign_in);
        password = (EditText) findViewById(R.id.password);
        confirm_password = (EditText) findViewById(R.id.confirm_password);
        btnRegister = (Button) findViewById(R.id.sign_up);

        btnRegister.setOnClickListener(this);

        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
            }
        });


    }

    @Override
    public void onClick(View v) {

        final String pseudo_user = pseudo.getText().toString().trim();
        final String email_user = email.getText().toString().trim();
        final String password_user = password.getText().toString().trim();
        final String password_check = confirm_password.getText().toString().trim();

        new Thread(new Runnable() {
            @Override
            public void run() {

                Log.d("Message", "run:Bouton ");

                try {
                    doRegisterUser(pseudo_user,email_user,password_user,password_check);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }).start();
    }
    public void doRegisterUser(String pseudo, String email, String password1, String password2) throws IOException {

        OkHttpClient client = new OkHttpClient();
        Log.d("Client", "run:client "+pseudo);

        RequestBody body = new FormBody.Builder()
                .add("pseudo",pseudo)
                .add("email",email)
                .add("password",password1)
                .add("confimation_password",password2)
                .build();
        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(url_Register)
                .post(body)
                .build();
        Log.d("request", "Request was created"+request);

        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.d("Response", "Response was created with sucess" + result);

            try {
                JSONObject user_informations = new JSONObject(result);
                String message = (String) user_informations.get("message");
                Log.d("JSONOBject", "User Informations" +message);

                if (message.equalsIgnoreCase("Inscription Reussite")){

                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                }
                else if (message.equalsIgnoreCase("Un compte existe déjà avec cet pseudo")) {
                    showToast("Un compte existe déjà avec cet pseudo");
                }
                else if (message.equalsIgnoreCase("Un compte existe déjà avec cet email")){
                    showToast("Un compte existe déjà avec cet email");
                }
                else if (message.equalsIgnoreCase("Les mots de passe doivent être identiques")){
                    showToast("Les mots de passe doivent être identiques");
                }
                else if (message.equalsIgnoreCase("Inscription Reussite")){
                    showToast("Inscription Reussite");
                }

                else if (message.equalsIgnoreCase("Erreur lors de l'inscription")){
                    showToast("Erreur lors de l'inscription");
                }

                else if (message.equalsIgnoreCase("Email invalide")) {
                    showToast("Email invalide");
                }
                else{
                    showToast("Veuillez remplir tous les champs ");
                }
            }
            catch (JSONException e){
                Log.d("JSonObject", "User Informations token failed");
                e.printStackTrace();
            }
        } catch (IOException e) {
            Log.d("Response", "Response was failed ");
            e.printStackTrace();
            showToast("CloudStorarage rencontre un problème. Veuillez verifier votre connexion internet");
        }

    }

    public void showToast(final String Text) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(RegisterActivity.this, Text, Toast.LENGTH_LONG).show();

            }
        });
    }

}


