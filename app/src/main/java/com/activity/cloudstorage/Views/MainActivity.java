package com.activity.cloudstorage.Views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.activity.cloudstorage.Controle.CallBackAction;
import com.activity.cloudstorage.Fragment.FichiersFragment;
import com.activity.cloudstorage.Fragment.MoiFragment;
import com.activity.cloudstorage.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayDeque;
import java.util.Deque;

public class MainActivity extends AppCompatActivity implements CallBackAction {
    private Toolbar toolbar_menu;
    Deque<Integer> integerDeque=new ArrayDeque<>(2);
    BottomNavigationView bottom_navigation;
    Boolean flag=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottom_navigation=findViewById(R.id.bottom_navigation);

            //NagigationButtom Fragment
        integerDeque.push(R.id.fichiers);
        loadFragment(new FichiersFragment());
        bottom_navigation.setSelectedItemId(R.id.fichiers);
        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id=item.getItemId();
                if (integerDeque.contains(id)){
                    if (id==R.id.fichiers){
                     //code........

                      if (integerDeque.size()!=1){

                          if (flag){
                              integerDeque.addFirst(R.id.fichiers);

                              flag=false;
                          }
                      }
                    }
                    integerDeque.remove(id);
                }
                integerDeque.push(id);
                loadFragment(getFragment(item.getItemId()));

                return true;
            }
        });

    }

    private Fragment getFragment(int itemId) {
        switch (itemId){
            case R.id.fichiers:
                bottom_navigation.getMenu().getItem(0).setChecked(true);
                return  new FichiersFragment();

            case R.id.my:
                bottom_navigation.getMenu().getItem(1).setChecked(true);
                return  new MoiFragment();

        }

        bottom_navigation.getMenu().getItem(0).setChecked(true);
        return new FichiersFragment();
    }

    private void loadFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment,fragment,fragment.getClass().getSimpleName())
                .commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        integerDeque.pop();

        if (!integerDeque.isEmpty()){
            loadFragment(getFragment(integerDeque.peek()));
        }else {
            finish();
        }
    }

        public String[] userInfo(){

            String [] all_user_info = {getIntent().getStringExtra("id"),
                    getIntent().getStringExtra("pseudo"),
                    getIntent().getStringExtra("email"),
                    getIntent().getStringExtra("password")};
            return all_user_info;
        }

    @Override
    public void Refresh() {
        Intent intent=new Intent(MainActivity.this, MainActivity.class);
        finish();
        overridePendingTransition(0,0);
        startActivity(intent);
        overridePendingTransition(0,0);
        Log.d("Refresh Action", "Refresh Sucess**********************************");

    }

    @Override
    public void onItemClick(String item) {
        ///Log.d("Item", "onItemClick: ----------------------------------"+item);

    }
}



