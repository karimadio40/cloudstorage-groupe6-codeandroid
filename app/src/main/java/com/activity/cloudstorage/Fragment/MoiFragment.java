package com.activity.cloudstorage.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activity.cloudstorage.Controle.LoginUser;
import com.activity.cloudstorage.R;
import com.activity.cloudstorage.Views.LoginActivity;
import com.activity.cloudstorage.Views.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MoiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MoiFragment extends Fragment {

    private TextView pseudo,email;
    private Button change_Password,logout;
    public MoiFragment() {
        // Required empty public constructor
    }


    public static MoiFragment newInstance(String param1, String param2) {
        MoiFragment fragment = new MoiFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    public String [] getUser() {
        MainActivity mainActivity=(MainActivity)getActivity();
        String user_Info[]=mainActivity.userInfo();
        return user_Info;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_moi, container, false);
        final Context context=view.getContext();
                email=view.findViewById(R.id.email);
                pseudo=view.findViewById(R.id.pseudo);
                logout =view.findViewById(R.id.logout);
                change_Password=view.findViewById(R.id.ChangePassword);
                final String[] user=getUser();
                Log.d("user", "user---------------------: "+user[1]);
                 pseudo.setText(user[1]);
                 email.setText(user[2]);

                 change_Password.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         final Dialog dialog =new Dialog(context);
                         LinearLayout layout = new LinearLayout(context);
                         layout.setOrientation(LinearLayout.HORIZONTAL);
                         layout.setMinimumHeight(500);
                         layout.setMinimumWidth(600);
                         final View dialogView=getLayoutInflater().inflate(R.layout.dialog_change_password,null);
                         Button No=dialogView.findViewById(R.id.no);
                         Button Yes=dialogView.findViewById(R.id.yes);
                         No.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 dialog.dismiss();
                             }
                         });

                         Yes.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 final Dialog dialogue =new Dialog(context);
                                 final View dialogView=getLayoutInflater().inflate(R.layout.change_password_layout,null);
                                 Button send=dialogView.findViewById(R.id.send);
                                 final EditText password1=dialogView.findViewById(R.id.pass_init);
                                 final EditText password2=dialogView.findViewById(R.id.password_new);


                                 send.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     public void onClick(View v) {
                                         new Thread(new Runnable() {
                                             @Override
                                             public void run() {

                                                 final String old_password=password1.getText().toString().trim();
                                                 final String new_password=password2.getText().toString().trim();
                                                LoginUser loginUser =new LoginUser();
                                                 loginUser.changePassword(context,user[1], old_password, new_password);
                                                 dialogue.dismiss();
                                             }
                                         }).start();
                                     }
                                 });
                                 dialog.dismiss();
                                 dialogue.setContentView(dialogView);
                                 dialogue.show();

                             }

                         });
                         dialog.setContentView(dialogView);
                         dialog.show();
                     }

                 });

                logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                 public void onClick(View v) {
                final Dialog dialog =new Dialog(context);
                LinearLayout layout = new LinearLayout(context);
                View dialogView=getLayoutInflater().inflate(R.layout.dialogue_logout,null);
                         Button No=dialogView.findViewById(R.id.btn_no);
                         Button Yes=dialogView.findViewById(R.id.btn_yes);
                        No.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        Yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent intent =new Intent(view.getContext(), LoginActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });

                        dialog.setContentView(dialogView);
                        dialog.show();
            }

        });

        return  view;
}



            }


