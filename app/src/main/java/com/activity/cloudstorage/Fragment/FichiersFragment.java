package com.activity.cloudstorage.Fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.widget.TextView;

import com.activity.cloudstorage.Controle.CallBackAction;
import com.activity.cloudstorage.Controle.LoginUser;
import com.activity.cloudstorage.Controle.FileItem;
import com.activity.cloudstorage.Adapter.FileItemAdapter;
import com.activity.cloudstorage.R;
import com.activity.cloudstorage.Utils.UploadFile;
import com.activity.cloudstorage.Utils.FileItemGestion;
import com.activity.cloudstorage.Utils.FilePathSender;
import com.activity.cloudstorage.Utils.GetAllFile;
import com.activity.cloudstorage.Views.MainActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FichiersFragment extends Fragment  implements View.OnClickListener, CallBackAction {
    private FloatingActionButton btn_img;
    private static final int MY_REQUEST_CODE_PERMISSION = 1000;
    private static final int MY_RESULT_CODE_FILECHOOSER = 2000;
    private static final String LOG_TAG = "AndroidExample";
    private ProgressBar progressBar;
    private GetAllFile getAllFile;
    private CallBackAction mcallback;
    private Toolbar toolbar_menu;

    public FichiersFragment() {
        // Required empty public constructor
    }


    public static FichiersFragment newInstance() {
        FichiersFragment fragment = new FichiersFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    public String [] getUser() {
        MainActivity mainActivity=(MainActivity)getActivity();
        String user_Info[]=mainActivity.userInfo();
        return user_Info;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] user_info = getUserInfo();
        switch (requestCode) {
            case MY_RESULT_CODE_FILECHOOSER:
                if (resultCode == Activity.RESULT_OK ) {
                    if(data != null)  {
                        Uri fileUri = data.getData();
                        String filePath = null;
                        try {
                            filePath = FilePathSender.getPath(this.getContext(),fileUri);
                            Log.i(LOG_TAG, "file: -------------------------------//" +filePath);
                            UploadFile uploadFile=new UploadFile();

                            uploadFile.doMultipartRequest(this.getContext(),filePath,user_info);
                            Log.i(LOG_TAG, "uploadfile: -------------------------------//" +uploadFile);

                        } catch (Exception e) {
                            Log.e(LOG_TAG,"Error: " + e);
                            Toast.makeText(this.getContext(), "Error: " + e, Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

//-----------------------------------------------------------------------------------------------
    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View result= inflater.inflate(R.layout.fragment_fichiers, container, false);
        toolbar_menu=result.findViewById(R.id.toolbar);
        toolbar_menu.inflateMenu(R.menu.toolbar_menu);
        toolbar_menu.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.upload:
                        doBrowseFile();

                    case R.id.save:
                        //code-------------

                    case R.id.delete:

                    default:
                return true;
            }}
        });

        getAllFile = new GetAllFile();
        new Thread(new Runnable() {
            @Override
            public void run() {

          final List<FileItem> fileItemList = getAllFile.GetFile();
                Log.d("Notre liste","Liste"+ fileItemList);
                try {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (fileItemList.size()<1){
                                Toast.makeText(getContext(),"Verrifier votre connexion intenet ",Toast.LENGTH_LONG).show();
                            }
                            else {
                                final String current_User[] = getUser();
                                final Context context = result.getContext();
                                final ListView file_list = (ListView) result.findViewById(R.id.liste);
                                FileItemAdapter adapter = new FileItemAdapter(context, fileItemList, current_User[1]);
                                file_list.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                file_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        FileItem currentFile = fileItemList.get(position);
                                        String checker_view = new FileItemGestion().getItemType(currentFile.getFileType(), currentFile.getFileName());
                                        ViewFileAction(context, checker_view, currentFile.getFileUrl(), currentFile.getFileName());

                                    }
                                });

                                file_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                                    @Override
                                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                                        final FileItem currentFile = fileItemList.get(position);
                                        final Dialog dialog = new Dialog(context);
                                        LinearLayout layout = new LinearLayout(context);
                                        layout.setOrientation(LinearLayout.VERTICAL);
                                        View mview = getLayoutInflater().inflate(R.layout.contextual_button, null);
                                        FileItemGestion fileItemGestion=new FileItemGestion();
                                        TextView download = (TextView) mview.findViewById(R.id.download);
                                        TextView delete = (TextView) mview.findViewById(R.id.supprimer);
                                        TextView Name_file=(TextView)mview.findViewById(R.id.nom_fichier);
                                        TextView taille_fichier=(TextView)mview.findViewById(R.id.taille);
                                        TextView sender_name=mview.findViewById(R.id.envoyeur);
                                        Name_file.setText(fileItemGestion.getFileName(currentFile.getFileName()));
                                        sender_name.setText(fileItemGestion.getSenderName(currentFile.getFileSender(),current_User[1]));
                                        taille_fichier.setText(fileItemGestion.getItemSize(currentFile.getFileSize()));
                                        download.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                Toast.makeText(context, "Téléchargement achevée", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                        delete.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                LoginUser loginUser=new LoginUser();
                                                loginUser.deleteFile(context,currentFile.getFileName(),currentFile.getFileId(),current_User[1]);
                                                Toast.makeText(context, "Suppression achevée", Toast.LENGTH_LONG).show();
                                            }
                                        });

                                        dialog.setContentView(mview);
                                        dialog.show();

                                        return true;
                                    }
                                });
                            }
                        }
                    });
                }catch (Exception e){
                    Toast.makeText(getView().getContext(),"Les fichiers enrégistrés dans" +
                            " la base de données ne pas disponibles actuelllemnt",Toast.LENGTH_LONG).show();
                }

            }
        }).start();

        progressBar = result.findViewById(R.id.progress);
        this.btn_img=result.findViewById(R.id.selector);
        this.btn_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askPermissionAndBrowseFile();
            }
        });

        return result;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.toolbar_menu,menu);
    }

    private void askPermissionAndBrowseFile()  {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) { // Level 23
            int permisson = ActivityCompat.checkSelfPermission(this.getContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (permisson != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_REQUEST_CODE_PERMISSION
                );
                return;
            }
        }
        this.doBrowseFile();
    }

    private void doBrowseFile()  {
        Intent chooseFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFileIntent.setType("*/*");
        // Only return URIs that can be opened with ContentResolver
        chooseFileIntent.addCategory(Intent.CATEGORY_OPENABLE);

        chooseFileIntent = Intent.createChooser(chooseFileIntent, "Choose a file");
        startActivityForResult(chooseFileIntent, MY_RESULT_CODE_FILECHOOSER);
    }

    // When you have the request results
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //
        switch (requestCode) {
            case MY_REQUEST_CODE_PERMISSION: {

                // Note: If request is cancelled, the result arrays are empty.
                // Permissions granted (CALL_PHONE).
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i(LOG_TAG, "Permission granted!");
                    Toast.makeText(this.getContext(), "Permission granted!", Toast.LENGTH_SHORT).show();

                    this.doBrowseFile();
                }
                // Cancelled or denied.
                else {
                    Log.i(LOG_TAG, "Permission denied!");
                    Toast.makeText(this.getContext(), "Permission denied!", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof CallBackAction){
            mcallback= (CallBackAction) context;
        }
        else{
            Log.d("Attach Action", "Attach Failed");
        }
    }

    @Override
    public void onClick(View v) {

    }

    public String [] getUserInfo() {
        MainActivity mainActivity=(MainActivity)getActivity();
        String user_Info[]=mainActivity.userInfo();
        return user_Info;
    }

    public void ViewFileAction(final Context context, final String fileType, final String fileUrl, final String filename){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (fileType.equalsIgnoreCase("image")){
                    Uri uri=Uri.parse(fileUrl);
                    Intent intent=new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(uri,"image/*");
                    startActivity(intent);
                }

                else if (fileType.equalsIgnoreCase("video")){
                    Uri uri=Uri.parse(fileUrl);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                MiseEnCache(fileUrl);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();

                    Intent intent=new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(uri,"video/*");
                    startActivity(intent);

                }

                else if (fileType.equalsIgnoreCase("audio")){
                    Uri uri=Uri.parse(fileUrl);
                    Intent intent=new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(uri,"audio/*");
                    startActivity(intent);

                }
                else {
                    Uri uri=Uri.parse(fileUrl);


                    Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(fileUrl));
                    intent.setDataAndType(uri,"application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    try {
                        startActivity(intent);
                    }
                    catch (ActivityNotFoundException e){
                        Toast.makeText(context,"No chance For You",Toast.LENGTH_SHORT).show();
                    }


                }

            }
        }).start();


    }

    @Override
    public void Refresh() {

    }

    @Override
    public void onItemClick(String item) {
        Log.d("Item", "onItemClick: ----------------------------------"+item);
    }

    public File DownloadFile(String url, String filename){
        File Directory=new File("/storage/emulated/0/CloudStorage/"+filename);
        if (!Directory.exists()){
            Directory.mkdirs();

            Log.d("directory", "dir--------------------------------: "+Directory);
        }
        Uri fileUri=Uri.parse(url);
        DownloadManager.Request request=new DownloadManager.Request(fileUri);
        if (android.os.Build.VERSION.SDK_INT>=android.os.Build.VERSION_CODES.N){
                    request.setDescription("Downloading ------------|")
                    .setTitle("Telechargement en cours ")
                    .setAllowedOverRoaming(true)
                    .setRequiresCharging(false)
                    .setDestinationUri(Uri.fromFile(Directory))
                    .setAllowedOverMetered(true)
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        }else {
            request =new DownloadManager.Request(fileUri)
                    .setAllowedOverRoaming(true)
                    .setDescription("Downloading ------------|")
                    .setTitle("Telechargement en cours ")
                    .setDestinationUri(Uri.fromFile(Directory))
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);

        }
        DownloadManager downloadfile=(DownloadManager)getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        downloadfile.enqueue(request);
        return Directory;

    }

    public String MiseEnCache(String fileurl) throws IOException {
        int cachesize=10*1024*10240 ;

        File cacheDir=new File(String.valueOf(Environment.getExternalStoragePublicDirectory("*/*")));
        Cache cache=new Cache(cacheDir,cachesize);
        OkHttpClient client=new OkHttpClient.Builder()
                .cache(cache)
                .build();
        Request request=new Request.Builder()
                .url(fileurl)
                .build();
        Response response1=client.newCall(request).execute();
        Log.d("response1", "response1--------------------------: "+response1);
        Response response2=client.newCall(request).execute();
        Log.d("response2", "response2--------------------------: "+response2);
        String results = response1.body().string();
        Log.d("Response", "Response was created with sucess" + results);

        return results;

    }



}

