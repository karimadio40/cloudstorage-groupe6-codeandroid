package com.activity.cloudstorage.Utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileItemGestion {
    public String getItemType(String initial_type,String file_name){
        String new_type ="";
        String ext = file_name.substring(file_name.indexOf(".")+1).toLowerCase();
        String ext_else =initial_type.substring(0,initial_type.indexOf("/")).toLowerCase().trim();
        Log.d("Type of file", "Elément de type" +ext_else);

        if (ext_else.equalsIgnoreCase("image")){
            new_type = ext_else;

        }
        else  if(ext_else.equalsIgnoreCase("video")){
            new_type =ext_else;


        }
        else if (ext_else.equalsIgnoreCase("audio")){
            new_type= ext_else;

        }
        else {

            new_type ="pdf";
        }


        return  new_type;
    }

    public String getItemSize(String filesize){
        String final_message = null;
        Integer final_size=null;
        Integer inter_size = Integer.parseInt(filesize);

        if (inter_size<=1000000){
            final_size = inter_size/1000;
            final_message = final_size.toString() +"Ko";
        }
        else{
            final_size = inter_size/1000000;
            final_message = final_size.toString() +"Mo";
        }

        return  final_message;

    }

    public String getFileName(String fileName){
        String final_name ="";
        String inter_name, suffix = "...";

        if (fileName.length()>25){
            inter_name =fileName.substring(0,23).toLowerCase().trim();
            final_name = inter_name+ suffix;
        }
        else{
            final_name = fileName;
        }

        return  final_name;
    }

    public String getSenderName(String senderName, String currentUserName){
        String final_senderName ="";

        if (senderName.equalsIgnoreCase(currentUserName)){
            final_senderName = "Moi";
        }
        else {
            final_senderName = senderName;
        }

        return  final_senderName;
    }


    /// Get Format Time

    public String getFormatTime(String uploadDate){
        String final_date ="";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
        Date date = new Date();

        try {

            Date file_date = simpleDateFormat.parse(uploadDate);
            final_date = GetDifference(file_date, date, uploadDate);

        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return final_date;
    }
    public String GetDifference(Date startDate, Date endDate,String upload_date){
        String inter_date ="";
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli *  60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli *  24;

        long elapsedDays = different/daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different/hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different/minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different/secondsInMilli;

        if (elapsedDays <1){
            if (elapsedHours>1){
                inter_date ="Envoyer il y a "+ String.valueOf(elapsedHours) +"h";
            }
            else{
                inter_date ="Envoyer il y a "+ String.valueOf(elapsedMinutes) +"min";
            }

        }

        else{
            inter_date ="Envoyer le "+ upload_date.substring(0,10);

        }

        return inter_date;
    }

    public String getFileExtension(String fileName){
        String ext = fileName.substring(fileName.indexOf(".")).toLowerCase();
        return ext;
    }
}
