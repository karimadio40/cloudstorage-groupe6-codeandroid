package com.activity.cloudstorage.Utils;

import android.util.Log;

import com.activity.cloudstorage.Controle.FileItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetAllFile {

    public List<FileItem> GetFile() {
        final List<FileItem> fileItemList = new ArrayList<FileItem>();
        final String get_file_url = "http://192.168.43.251/AndroidTP/Get_all_file.php";

        OkHttpClient client = new OkHttpClient();
        Log.d("Client", "Client create");

        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(get_file_url)
                .build();
        Log.d("request", "Request was created" + request);

        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();

            try {
                JSONArray file_list = new JSONArray(result);
                for (int i = 0; i < file_list.length(); i++) {
                    JSONObject file_item = (JSONObject) file_list.get(i);
                    fileItemList.add(new FileItem(file_item.getString("name_file"),
                            file_item.getString("file_type"),
                            file_item.getString("file_size"),
                            file_item.getString("sendername"),
                            file_item.getString("file_url"),
                            file_item.getString("upload_date"),
                            file_item.getString("id")
                            ));
                }

                Log.d("Result", "GetFile: Your JSONObject was created " + fileItemList.get(1).getFileName());
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("JSON", "JSON: Can not get JSONObject " + e);
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Response", "GetFile: Your response was failed ");

        }


        return fileItemList;
    }

}
