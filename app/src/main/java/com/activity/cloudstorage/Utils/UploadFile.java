package com.activity.cloudstorage.Utils;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.activity.cloudstorage.Controle.LoginUser;
import com.activity.cloudstorage.Fragment.FichiersFragment;
import com.activity.cloudstorage.Views.LoginActivity;
import com.activity.cloudstorage.Views.MainActivity;
import com.activity.cloudstorage.Views.RegisterActivity;


import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import okhttp3.Cache;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UploadFile  {
    ProgressBar progressBar;

    public void doMultipartRequest (final Context context, String path, final String[] user_pseudo){
                            final File file= new File(path);


            if (file.isFile()){

                Log.d("OKhttp REQUEST", "FileName:*********************** "+file.getName());
                Log.d("size", "SIZE**********************************/: "+file.length());
                new  Thread(new Runnable() {
                    @Override
                    public void run() {
                        doActualRequest(context,file,user_pseudo);
                    }
                }).start();


            }

        }

    private void doActualRequest(Context context,File file,String[] user_info) {
        String url_save_file = "http://192.168.43.251/AndroidTP/File_upload.php";
        OkHttpClient client=new OkHttpClient();
        Log.d("Client", "Client create with success");
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext = file.getName().substring(file.getName().indexOf(".")+1).toLowerCase();
        String type = mime.getMimeTypeFromExtension(ext);
        if (type==null){
             type="Audio/mp4";
        }
        Log.d("Type", "The type of selected file ------------------------------------" + type);
        MultipartBody body=new MultipartBody.Builder()
        .setType(MultipartBody.FORM)
                .addFormDataPart("id",user_info[0])
                .addFormDataPart("pseudo",user_info[1])
                .addFormDataPart("email",user_info[2])
                .addFormDataPart("fichier",file.getName(),
                        RequestBody.create(MediaType.parse(type),file))
                .build();
        Log.d("MultipartBody", "Multipart Body create with success");

        Request request=new Request.Builder()
                .url(url_save_file)
                .header("Accept","Application/json")
                .header("content-type","Application/json")
                .post(body)
                .build();
        Log.d("Request", "Request create with success " +request);
        try {
            Response response = client.newCall(request).execute();
            String results = response.body().string();
            Log.d("Response", "Response was created with sucess" + results);

            try {
                JSONObject upload_back= new JSONObject(results);
                String message = (String) upload_back.get("message");
                Log.d("JSONOBject", "User Informations" +message);

                if (message.equalsIgnoreCase("Upload success")){
                    showToast(context,"Téléchargement réussi");

                }
                else if (message.equalsIgnoreCase("Upload failed")) {
                    showToast(context,"Téléchargement échoué");

                }

            }catch (Exception e){
                e.printStackTrace();
                showToast(context,"Téléchargement échoué");

            }

        }
        catch (IOException e) {
            Log.d("Response", "Response was failed ");
            e.printStackTrace();
            showToast(context,"Téléchargement échoué");

        }

    }

    public void showToast(final Context context, final String message) {

        Handler handler=new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });

    }

}

