package com.activity.cloudstorage.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.activity.cloudstorage.Controle.FileItem;
import com.activity.cloudstorage.R;
import com.activity.cloudstorage.Utils.FileItemGestion;
import com.bumptech.glide.Glide;
import java.util.List;

public class FileItemAdapter extends BaseAdapter {

    private Context context;
    private List<FileItem> fileItemList;
    private LayoutInflater inflater;
    private  String current_userName;

    public FileItemAdapter(Context context, List<FileItem> fileItemList,String current_userName) {
        this.context = context;
        this.fileItemList = fileItemList;
        this.inflater = LayoutInflater.from(context);
        this.current_userName=current_userName;
    }

    @Override
    public int getCount() {
        return fileItemList.size();
    }

    @Override
    public FileItem getItem(int position) {
        return fileItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0 ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.detail_fichiers,null);

        //Get Current File Informations
        FileItem currentFile = getItem(position);
        String fileName =currentFile.getFileName();
        String fileSize = currentFile.getFileSize();
        String fileSender = currentFile.getFileSender();
        String fileType = currentFile.getFileType();
        String fileDate = currentFile.getFileDate();
        String fileUrl = currentFile.getFileUrl();


        // Get Container Information
        TextView fileNameContainer = convertView.findViewById(R.id.file_name);
        String checker_Name =new  FileItemGestion().getFileName(fileName);
        fileNameContainer.setText(checker_Name);
        TextView exttentionContainer=convertView.findViewById(R.id.extention);
        String checker_extention=new  FileItemGestion().getFileExtension(fileName);
        exttentionContainer.setText(checker_extention);
        String checker_size = new FileItemGestion().getItemSize(fileSize);
        TextView fileSizeContainer = convertView.findViewById(R.id.file_size);
        fileSizeContainer.setText(checker_size);
        TextView fileSenderContainer = convertView.findViewById(R.id.sender);
        String checker_sender=new FileItemGestion().getSenderName(fileSender,current_userName);

        if (checker_sender.equalsIgnoreCase("Moi")){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fileSenderContainer.setTextAppearance(R.style.TextAppearance_AppCompat_Medium);
                fileSenderContainer.setTextColor(Color.rgb(228,243,16));
            }
            fileSenderContainer.setText(checker_sender);
        }
        else{
            fileSenderContainer.setText(checker_sender);
        }



        fileSenderContainer.setText(checker_sender);
        String checker_image =new FileItemGestion().getItemType(fileType,fileName);
        ImageView fileImage = convertView.findViewById(R.id.file_image);

        if(checker_image.equalsIgnoreCase("image")){
            Glide.with(context).load(Uri.parse(fileUrl)).into(fileImage);
        }
        else {
            String ressourceName ="ic_"+checker_image;
            int res =context.getResources().getIdentifier(ressourceName,"drawable",context.getPackageName());
            fileImage.setImageResource(res);
        }



        TextView fileDateContainer = convertView.findViewById(R.id.file_date);
        String checker_Date =new  FileItemGestion().getFormatTime(fileDate);
        fileDateContainer.setText(checker_Date);

        return convertView;
    }


}
