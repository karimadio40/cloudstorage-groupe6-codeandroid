package com.activity.cloudstorage.Controle;

public class FileItem {

    private String fileName;
    private  String fileSize;
    private String fileSender;
    private  String fileType;
    private  String fileUrl;
    private  String fileDate;
    private  String fileId;


    public FileItem(String fileName,String fileType, String fileSize, String fileSender, String fileUrl,String fileDate,String fileId) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.fileSender = fileSender;
        this.fileUrl = fileUrl;
        this.fileDate = fileDate;
        this.fileId=fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileDate() {
        return fileDate;
    }

    public String getFileType() {
        return fileType;
    }



    public String getFileSize() {
        return fileSize;
    }

public  String getFileId(){return  fileId;}

    public String getFileSender() {
        return fileSender;
    }

    public String getFileUrl() {
        return fileUrl;
    }


}
