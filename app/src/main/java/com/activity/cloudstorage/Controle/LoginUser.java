package com.activity.cloudstorage.Controle;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.activity.cloudstorage.Fragment.MoiFragment;
import com.activity.cloudstorage.Views.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginUser {
    private String pseudo_user;
    private String password_user;
    private String email_user;
    private String id_user;
    private String message;


    final String login_url = "http://192.168.43.251/AndroidTP/connexion.php";

    public LoginUser(){
    }

    public LoginUser(String pseudo_user, String password_user) {
        this.pseudo_user = pseudo_user;
        this.password_user = password_user;
    }

    public String getEmail_user() {
        return email_user;
    }

    public String getId_user() {
        return id_user;
    }

    public String getPseudo_user() {
        return pseudo_user;
    }

    public String getPassword_user() {
        return password_user;
    }

    public void setEmail_user(String email_user) {
        this.email_user = email_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public void setPseudo_user(String pseudo_user) {
        this.pseudo_user = pseudo_user;
    }

    public void setPassword_user(String password_user) {
        this.password_user = password_user;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void doLoginUser(final Context context, String pseudo, String password) {

        OkHttpClient client = new OkHttpClient();
        Log.d("Client", "run:client " + pseudo);

        RequestBody body = new FormBody.Builder()
                .add("pseudo", pseudo)
                .add("password", password)
                .build();
        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(login_url)
                .post(body)
                .build();

        Log.d("Request", "Request was created with success " + request);

        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.d("Response", "Response was created with sucess" + result);

            try {
                JSONObject user_informations = new JSONObject(result);
                final String message=((String) user_informations.get("message")) ;
                Log.d("JSONOBject", "User Informations" + message);

                if (message.equalsIgnoreCase("Welcome")) {
                    setId_user((String) user_informations.get("id"));
                    setPseudo_user((String) user_informations.get("pseudo"));;
                    setEmail_user ((String)user_informations.get("email"));
                    setPassword_user((String) user_informations.get("password"));
                    Log.d("email","     :"+getEmail_user());
                    Intent intent=new Intent(context, MainActivity.class);
                    intent.putExtra("id",getId_user());
                    intent.putExtra("email",getEmail_user());
                    intent.putExtra("pseudo",getPseudo_user());
                    intent.putExtra("password",getPassword_user());
                    context.startActivity(intent);
                    showToast(context,"Bienvenue dans CloudStorage : Les Meilleurs souvenirs de la vie");

                } else if (message.equalsIgnoreCase("Incorrect Password")) {
                    setMessage("Mot de passe incorrect");

                           showToast(context, "Incorrect Password");

                    } else if (message.equalsIgnoreCase("Incorrect Pseudo")) {
                    setMessage("Pseudo ou mot de passe incorrect");

                       showToast(context, "Pseudo ou mot de passe incorrect");

                } else {
                    setMessage("Veuillez remplir tous les champs ");

                          showToast(context, "Veuillez remplir tous les champs ");

               }

            } catch (JSONException e) {
                Log.d("JSonObject", "User Informations token failed");
                e.printStackTrace();
            }

        } catch (IOException e) {
            Log.d("Response", "Response was failed ");
            e.printStackTrace();

            showToast(context,"CloudStorarage rencontre un problème. Veuillez verifier votre connexion internet");

        }

    }

    public String PasswordUpdating(Context context,String pseudo,String password){

        String final_message = "nothing yet";
        String url_password = "http://192.168.43.251/AndroidTP/PasswordForget.php";
        OkHttpClient client = new OkHttpClient();
        Log.d("Client", "run:client " + pseudo);


        RequestBody body = new FormBody.Builder()
                .add("pseudo",pseudo)
                .add("password",password)
                .build();
        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(url_password)
                .post(body)
                .build();
        Log.d("request", "Request was created"+request);

        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.d("Response", "Response was created with sucess" + result);

            try {
                JSONObject user_informations = new JSONObject(result);
                String message = (String) user_informations.get("message");
                Log.d("JSONOBject", "User Informations" +message);

                // Success Message
                if (message.equalsIgnoreCase("Change success")){
                   showToast(context, "Mot de passe changé avec succès ! " + pseudo + " Connectez vous !");
                    final_message = "win";
                }
                // Pseudo Error
                else if (message.equalsIgnoreCase("Not existing pseudo")) {
                   showToast(context,"Aucun compte n'a été retrouvé avec ce pseudo");
                }

                else{
                showToast(context,"Veuillez remplir tous les champs ");
                }
            }
            catch (JSONException e){
                Log.d("JSonObject", "User Informations token failed");
                e.printStackTrace();
            }

        }

        catch (IOException e) {
            Log.d("Response", "Response was failed ");
            e.printStackTrace();
            showToast(context,"Impossible de se connecter à  Internet. Veuillez" +
                    " réessayer ultérieurement !");
        }

        return  final_message;
    }

    public void deleteFile(final Context context, final String fileName, final String id, final String currentUserName){

        new Thread(new Runnable() {
            @Override
            public void run() {

                String url_Delated_file = "http://192.168.43.251/AndroidTP/Deleted_file.php";
                OkHttpClient client = new OkHttpClient();
                Log.d("Client", "run:client for delete File With id " + id);


                RequestBody body = new FormBody.Builder()
                        .add("id",id)
                        .add("currentUserName",currentUserName)
                        .build();
                Log.d("Formbody", "run:Formbody ");

                Request request = new Request.Builder()
                        .url(url_Delated_file)
                        .post(body)
                        .build();
                Log.d("request", "Request was created"+request);


                try {
                    Response response = client.newCall(request).execute();
                    String result = response.body().string();
                    Log.d("Response", "Response was created with sucess" + result);

                    try {
                        JSONObject user_informations = new JSONObject(result);
                        String message = (String) user_informations.get("message");
                        Log.d("JSONOBject", "User Informations" +message);

                        // Success Message
                        if (message.equalsIgnoreCase("Delete Success")){
                            showToast(context, "Fichier  " + fileName + " Supprimé");

                        }
                        // Pseudo Error
                        else if (message.equalsIgnoreCase("Unable Delete")) {
                            showToast(context,"Vous ne pouvez pas supprimer ce fichier car vous n'etes pas l'envoyeur.");
                        }

                        else{
                            showToast(context,"Erreur lors de la suppression");
                        }
                    }
                    catch (JSONException e){
                        Log.d("JSonObject", "User Informations token failed");
                        e.printStackTrace();
                    }

                }

                catch (IOException e) {
                    Log.d("Response", "Response was failed ");
                    e.printStackTrace();
                    showToast(context,"Impossible de se connecter à  Internet. Veuillez réessayer ultérieurement !");
                }


            }
        }).start();

    }

    public void changePassword(Context context,String pseudo,String password_init,String password_new )  {
        OkHttpClient client = new OkHttpClient();
         String url="http://192.168.43.251/AndroidTP/ChangePassword.php";

        RequestBody body = new FormBody.Builder()
                .add("pseudo", pseudo)
                .add("password_init", password_init)
                .add("password_new",password_new)
                .build();
        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Log.d("Request", "Request was created with success " + request);

        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.d("Response", "Response was created with sucess" + result);

            try {
                JSONObject user_informations = new JSONObject(result);
                final String message = ((String) user_informations.get("message"));
                final  String password=((String)user_informations.get(("password")));
                Log.d("JSONOBject", "User Informations************************" + message);
                Log.d("JSONOBject", "User Informations*************************" + password);
                showToast(context,"Mot de passe changer avec succes");

                if (message.equalsIgnoreCase("success")){
                    showToast(context,"Mot de passe changer avec succes");

                }
            showToast(context,"ERReur : Verifier votre mot de pass initial");
            }catch (JSONException e){
                e.printStackTrace();
            }
        } catch (IOException e ) {
            e.printStackTrace();
        }
    }


    public void showToast(final Context context,final String message) {

        Handler handler=new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });


    }
    }
